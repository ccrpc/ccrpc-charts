# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.9.3](https://gitlab.com/ccrpc/ccrpc-charts/compare/v0.9.2...v0.9.3) (2020-07-10)

### [0.9.2](https://gitlab.com/ccrpc/ccrpc-charts/compare/v0.9.1...v0.9.2) (2020-06-03)


### Bug Fixes

* **csv:** fix row/column filters with 2-digit numbers ([ab8d5b1](https://gitlab.com/ccrpc/ccrpc-charts/commit/ab8d5b1312d8baae5a8c05828f5c526fd4e5783e))

### [0.9.1](https://gitlab.com/ccrpc/ccrpc-charts/compare/v0.9.0...v0.9.1) (2020-06-03)


### Bug Fixes

* change invalid data exception to a warning ([0f2ce84](https://gitlab.com/ccrpc/ccrpc-charts/commit/0f2ce847a1bcaa403af6ed6f56fdddbad71e6bd9))

## [0.9.0](https://gitlab.com/ccrpc/ccrpc-charts/compare/v0.8.2...v0.9.0) (2020-05-20)


### ⚠ BREAKING CHANGES

* **metadata:** The default behavior of the `download` property is now to download the data as provided by the data source (i.e., in its filtered state). To download the raw CSV file, set `download` to the CSV URL.

### Features

* add pluggable data sources for charts and tables ([a3005a04](https://gitlab.com/ccrpc/ccrpc-charts/commit/a3005a04ce1dc2f6a8e6a648d8cc99c9d3565eb1)), closes [#17](https://gitlab.com/ccrpc/ccrpc-charts/issues/17) [#19](https://gitlab.com/ccrpc/ccrpc-charts/issues/19) [#20](https://gitlab.com/ccrpc/ccrpc-charts/issues/20) [#21](https://gitlab.com/ccrpc/ccrpc-charts/issues/21) [#22](https://gitlab.com/ccrpc/ccrpc-charts/issues/22) [#24](https://gitlab.com/ccrpc/ccrpc-charts/issues/24)
* set default value for `data` to an empty array ([dd5ce78](https://gitlab.com/ccrpc/ccrpc-charts/commit/dd5ce78a3fa35ca4ea83c03d27fc0b9bb768bc09)), closes [#12](https://gitlab.com/ccrpc/ccrpc-charts/issues/12) [#23](https://gitlab.com/ccrpc/ccrpc-charts/issues/23) [#26](https://gitlab.com/ccrpc/ccrpc-charts/issues/26)
* **chart:** add options property for custom chart types ([d4431b1](https://gitlab.com/ccrpc/ccrpc-charts/commit/d4431b1f3013e0ca81b901a8e66205dc08f7153d)), closes [#27](https://gitlab.com/ccrpc/ccrpc-charts/issues/27)
* **chart:** update chart on componentDidRender ([9d24d69](https://gitlab.com/ccrpc/ccrpc-charts/commit/9d24d69bf441fa50ee9bfe5c27c894f0c83ce13c)), closes [#14](https://gitlab.com/ccrpc/ccrpc-charts/issues/14)
* **dataset:** add line tension property to dataset and chart ([a6d3e3e](https://gitlab.com/ccrpc/ccrpc-charts/commit/a6d3e3e002eab037e7a7edd91255b8177eec08e8)), closes [#18](https://gitlab.com/ccrpc/ccrpc-charts/issues/18)
* **metadata:** defer creation of CSV until link is clicked ([034a124](https://gitlab.com/ccrpc/ccrpc-charts/commit/034a124d6161136c766f56005de19e817bdd223e)), closes [#29](https://gitlab.com/ccrpc/ccrpc-charts/issues/29)


### Bug Fixes

* handle null and undefined in CSV export ([a581b4f](https://gitlab.com/ccrpc/ccrpc-charts/commit/a581b4fba7a64b5a6f96941672f2ffc1bdec3e48)), closes [#26](https://gitlab.com/ccrpc/ccrpc-charts/issues/26)
* remove unused query string parameters in example ([287b542](https://gitlab.com/ccrpc/ccrpc-charts/commit/287b54243d7a99fa43ae2b85dd94b3996dafc6ae))

### [0.8.2](https://gitlab.com/ccrpc/ccrpc-charts/compare/v0.8.1...v0.8.2) (2020-02-12)


### Features

* allow non-numeric data in custom chart types ([2340b97](https://gitlab.com/ccrpc/ccrpc-charts/commit/2340b9730e1beeb7528c6b308e00aa55215b9360))

### 0.8.1 (2020-02-06)

## [0.8.0] - 2020-01-31
- Use `@stencil/core@1.8.6` and `chart.js@2.9.3`.

## [0.7.0] - 2019-08-14
- Use `@stencil/core@1.2.4`.
- Added `maintain-aspect-ratio` attribute to `rpc-chart`.
- Changed types of `height` and `width` attributes on `rpc-chart` from
  numbers to strings.

## [0.6.0] - 2019-04-29
- Fixed race condition in charts with `rpc-dataset` and `rpc-scale` children.
- Added `time-unit` property to `rpc-scale`.
- Use Chart.js 2.8.0 and Stencil 0.18.1.
- Fixed handling of formatting characters in `pyramid-bar` chart type.

## [0.5.0] - 2019-03-12
- Addition of `ChartType` classes, which can be used to modify base `Chart`
  types, allowing the creating of custom charts.
- Addition of the custom chart type `pyramidBar`.

## [0.4.0] - 2019-02-26
- Allowed `rpc-dataset` style properties to be arrays.
- Fixed detection of manually-created `rpc-scale` elements.
- Added `wrap` attribute to `rpc-chart` to wrap labels.

## [0.3.0] - 2019-01-29
- Added `data` attribute to `rpc-table` and `rpc-chart`.
- Strip leading and trailing whitespace when string parameters are converted
  to arrays (#2).
- Made the `grid-lines` attribute on `rpc-chart` impact created scales (#1).
- Added `x-min`, `x-max`, `y-min`, and `y-max` properties to `rpc-chart` (#3).
- Use Chart.js 2.7.3 and Stencil 0.17.0.

## [0.2.1] - 2018-10-25
- Fixed a packaging error.

## [0.2.0] - 2018-10-25
- Added table componment.

## [0.1.0] - 2018-10-24
- Initial release
