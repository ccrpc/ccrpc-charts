# CCRPC Charts and Visualizations

Charts and visualizations for the Champaign County Regional Planning Commission
based on [Chart.js](https://chartjs.org/). To see the library in action,
visit [the examples page](https://ccrpc.gitlab.io/ccrpc-charts/).

## Documentation

### Components
* [`rpc-chart`](src/components/chart)
* [`rpc-dataset`](src/components/dataset)
* [`rpc-scale`](src/components/scale)
* [`rpc-table`](src/components/table)

### Advanced Topics
* [Custom chart types](src/components/chart/chart-types)
* [Data sources](src/components/datasource)

## Credits
CCRPC Charts was developed by Matt Yoder and Edmond Lai for the
[Champaign County Regional Planning Commission](https://ccrpc.org/).

## License
CCRPC Charts is available under the terms of the [BSD 3-clause
license](https://gitlab.com/ccrpc/ccrpc-charts/blob/master/LICENSE.md).
