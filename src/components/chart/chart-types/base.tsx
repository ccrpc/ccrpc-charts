import { ChartConfiguration, ChartType as ChartTypeString } from 'chart.js'
import { ChartType } from '../interface';



export class BaseChartType implements ChartType {

  type: ChartTypeString = 'bar';

  modifySourceData(_data: any[][], _options?: any) {

  }

  modifyElement(_el: HTMLElement, _options?: any) {

  }

  modifyConfig(_config: ChartConfiguration, _options?: any) {

  }

}
