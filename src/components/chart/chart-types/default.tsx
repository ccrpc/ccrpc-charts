import { ChartType as ChartTypeString } from 'chart.js'
import { BaseChartType } from './base';


export class DefaultChartType extends BaseChartType {
  constructor(chartType: ChartTypeString) {
    super();
    this.type = chartType;
  }
}
