import { ChartConfiguration, ChartType as ChartTypeString, ChartDataSets } from 'chart.js'
import { BaseChartType } from './base';


export class PyramidBarChartType extends BaseChartType {

  type: ChartTypeString = 'horizontalBar';

  modifySourceData(data: any[][]) {
    data = data.map((row: any[], i: number) => {
      // set the first row to negative, unless it's the column header
      if (i !== 0) row[1] = -row[1];
      return row ;
    });
  }

  getXAxisLimit(datasets: ChartDataSets[]) {

    const min_data =  datasets[0].data;
    const max_data =  datasets[1].data;

    let min = min_data[0];
    let max = max_data[0];
    for (let i=0; i < min_data.length; i ++ ) {
      if (min_data[i] < min) {
        min = min_data[i];
      }
      if (max_data[i] > max) {
        max = max_data[i];
      }
    }
    // cast min/max to number to allow arithmetic
    min = min as number * -1;
    max = max as number;

    const xAxisAbsMax = Math.round(Math.max(min, max));
    const buffer = Math.round(0.25 * xAxisAbsMax);

    return xAxisAbsMax + buffer;
  }

  modifyConfig(config: ChartConfiguration) {

    const xAxis = config['options'].scales.xAxes[0];
    // alter xAxes ticks to show absolute value
    xAxis.ticks.callback = (element) => Math.abs(element);

    // set stacked on yAxes to true
    config['options'].scales.yAxes[0].stacked = true;

    if (xAxis.ticks.min === undefined || xAxis.ticks.max === undefined) {
      // set x-min and x-max
      const xAxisLimit = this.getXAxisLimit(config['data'].datasets as any);
      xAxis.ticks.min = -1 * xAxisLimit;
      xAxis.ticks.max = xAxisLimit;
    } else {
      xAxis.ticks.min = -1 * xAxis.ticks.min;
    }

    // alter tooltip xLable to show absolute value
    config['options'].tooltips.callbacks = {
      label: (tooltipItem) =>
        Math.abs(parseFloat(tooltipItem.xLabel.toString())).toString()
    }
  }

}
