# Custom Chart Types
Custom chart types allow the creation of customized versions of the
[chart types provided by Chart.js](https://www.chartjs.org/samples/latest/).
CCRPC Charts includes one custom chart type and provides an API for creating
new custom types.

## Built-in Custom Chart Types

### Pyramid Bar Chart
The `pyramidBar` chart type creates the type of stacked bar chart typically
used for population pyramids:

```html
<rpc-chart url="data.csv"
  chart-title="Pyramid Bar Chart"
  type="pyramidBar"
  x-label="Score"
  y-label="Year"
  x-min="7"
  x-max="7"
  ></rpc-chart>
```

The `x-min` and `x-max` attributes correspond to the left and right side of
the pyramid, respectively. If `x-min` and `x-max` are not supplied, default
values are be calculated.

## Creating Custom Chart Types
Custom chart types can be created by extending the `BaseChartTypes` class.
`BaseChartTypes provides three hook methods that can be overridden to
customize the appearance and behavior of the chart type:

```ts
import { ChartConfiguration, ChartType } from 'chart.js'
import { BaseChartType } from '@ccrpc/charts';

class MyChartType extends BaseChartType {

  /**
   * The underlying default chart type that will be reported to Chart.js.
   */
  type: ChartType = 'bar';

  /**
   * Modify the data structure returned by the data source.
   */
  modifySourceData(data: any[][]) {}

  /**
   * Modify the element to add DOM elements outside the chart canvas.
   */
  modifyElement(el: HTMLElement) {}

  /**
   * Modify the final chart configuration sent to Chart.js.
   */
  modifyConfig(config: ChartConfiguration) {}

}
```

New custom chart type classes have to be registered with CCRPC Charts before
they can be used in `rpc-chart` components:

```ts
import { chartTypeRegistry } from '@ccrpc/charts';

chartTypeRegistry.register('myType', new MyChartType);
```
