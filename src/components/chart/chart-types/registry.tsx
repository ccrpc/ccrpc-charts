import { ChartType } from '../interface';
import { DefaultChartType } from './default';
import { PyramidBarChartType } from './pyramid-bar';


class ChartTypeRegistry {
  defaultType : string;
  types : { [key: string]: ChartType; } = {};

  public get(name: string) {
    return this.types[name] || this.types[this.defaultType];
  }

  public register(name: string, type: ChartType, 
      makeDefault: boolean = false) {
    this.types[name] = type;
    if (this.defaultType === undefined || makeDefault) this.defaultType = name;
  }

  public unregister(name: string) {
    if (name in this.types) delete this.types[name];
  } 
}

export const chartTypeRegistry = new ChartTypeRegistry();

chartTypeRegistry.register('bar', new DefaultChartType('bar'));
chartTypeRegistry.register('line', new DefaultChartType('line'));
chartTypeRegistry.register('horizontalBar',
  new DefaultChartType('horizontalBar'));
chartTypeRegistry.register('scatter', new DefaultChartType('scatter'));
chartTypeRegistry.register('pie', new DefaultChartType('pie'));
chartTypeRegistry.register('doughnut', new DefaultChartType('doughnut'));
chartTypeRegistry.register('pyramidBar', new PyramidBarChartType);
