import { h, Component, Element, Prop, State, Watch } from '@stencil/core';
import { default as ChartJS } from 'chart.js';
import { ChartOptions, InteractionMode, PositionType, ScaleType }
  from 'chart.js';
import { removeUndefined, rpcColor, setOpacity, toArray,
  getDataForElement } from '../../utils';
import { ChartType } from './interface';
import { chartTypeRegistry } from './chart-types/registry';


ChartJS.defaults.global.defaultFontFamily =
  "'Open Sans', 'Helvetica Neue', 'Helvetica', 'Arial', sans-serif";
ChartJS.defaults.global.defaultFontColor = 'black';


@Component({
  tag: 'rpc-chart',
  styleUrl: 'chart.css',
  shadow: true
})
export class Chart {
  canvas?: HTMLCanvasElement;
  chart?: ChartJS;
  chartType?: ChartType;
  
  @Element() el: HTMLElement;

  @State() parsedData?: string[][] = [];

  /**
   * Length of animations in seconds.
   */
  @Prop() animationDuration: number = 0;

  /**
   * Ratio of width to height. It is ignored if the `height` and `width`
   * properties are set.
   */
  @Prop() aspectRatio: number = 2;

  /** 
   * Chart height in pixels or percent (e.g., `400px` or `100%`).
   */
  @Prop() height: string;

  /**
   * Chart width in pixels or percent (e.g., `400px` or `100%`).
   */
  @Prop() width: string;

  /**
   * Scale the chart to fit the width of its container.
   */
  @Prop() responsive: boolean = true;

  /**
   * Maintain the original aspect ratio when resizing.
   */
  @Prop() maintainAspectRatio: boolean = true;

  /**
   * An array of series labels, or a comma-separated string containing the
   * labels. If labels are not provided, the first value in each row is used.
   */
  @Prop({mutable: true}) labels: string[] | string[][];

  /**
   * Data source URL. For more information, see [Data Sources](../datasource).
   */
  @Prop() url: string;

  /**
   * An array of row arrays, or a JSON-formatted string containing an array
   * of row arrays. If the `url` property is set, `data` is ignored.
   */
  @Prop() data: string | any[][] = [];

  /** 
   * Show the data download link. If a URL is provided, it is used as the
   * URL for the download. Otherwise, if the value of download is `true`,
   * the data returned by the data source are converted to a CSV.
   */
  @Prop() download: boolean | string = true;

  /**
   * @deprecated See [Data Sources](../datasource) for URL-based filtering
   * options.
   */
  @Prop() rows: string;

  /**
   * @deprecated See [Data Sources](../datasource) for URL-based filtering
   * options.
   */
  @Prop() columns: string;

  /**
   * @deprecated See [Data Sources](../datasource) for URL-based filtering
   * options.
   */
  @Prop() switch: boolean;

  /**
   * Type of chart: `bar`, `bubble`, `doughnut`, `horizontalBar`, `line`,
   * `pie`, `polarArea`, `radar`, or `scatter`.
   * See also [Custom Chart Types](./chart-types).
   */
  @Prop() type: string = 'bar';

  /**
   * Options object for custom chart types. It is passed to the custom
   * chart type hooks and can be used to alter the behavior of the chart.
   */
  @Prop() typeOptions: any;

  /**
   * Title displayed above the chart.
   */
  @Prop() chartTitle: string;

  /**
   * Summary text that appears below the chart.
   */
  @Prop() description: string;

  /**
   * Name of the data source displayed below the chart.
   */
  @Prop() source: string;

  /**
   * URL for the data source link.
   */
  @Prop() sourceUrl: string;

  /**
   * Show the legend.
   */
  @Prop() legend: boolean = true;

  /**
   * Side of the chart where the legend will be displayed: `left`, `right`,
   * `bottom`, or `top`.
   */
  @Prop() legendPosition: PositionType = 'top';

  /**
   * Array or comma-separated string of color names or hexidecimal values.
   */
  @Prop() colors: string | string[] = [
    'red', 'blue', 'lime', 'orange', 'indigo', 'green',
    'violet', 'yellow', 'gray', 'brown'];

  /**
   * Show grid lines on charts with an x-axis and y-axis.
   */
  @Prop() gridLines: boolean;

  /**
   * Axis label for the x-axis.
   */
  @Prop() xLabel: string;

  /**
   * Maximum tick value for the x-axis.
   */
  @Prop() xMax: number;

  /**
   * Minimum tick value for the x-axis.
   */
  @Prop() xMin: number;

  /**
   * X-axis type: `category`, `linear`, `logarithmic`, `radialLinear`,
   * or `time`
   */
  @Prop() xType: ScaleType;

  /**
   * Axis label for the y-axis.
   */
  @Prop() yLabel: string;

  /**
   * Maximum tick value for the y-axis.
   */
  @Prop() yMax: number;

  /**
   * Minimum tick value for the y-axis.
   */
  @Prop() yMin: number;

  /**
   * Y-axis type: `category`, `linear`, `logarithmic`, `radialLinear`,
   * or `time`
   */
  @Prop() yType: ScaleType;

  /**
   * Maximum number of characters per line in axis category labels.
   */
  @Prop() wrap: number;

  /**
   * Bezier curve tension of the line. Use a line tension of `0` to draw
   * straight lines.
   */
  @Prop() lineTension: number;

  /**
   * Line width for the `line` chart type.
   */
  @Prop() lineWidth: number = 2;

  /**
   * Where to begin shading for an area chart created using the `line` chart
   * type: `start`, `end`, `origin`, or `false` (no shading).
   */
  @Prop() fill: boolean | number | string = false;

  /**
   * Stack bars in `bar` or `horizontalBar` chart types.
   */
  @Prop() stacked: boolean;

  /**
   * Show tooltips when the cursor hovers over the chart.
   */
  @Prop() tooltip: boolean = true;

  /**
   * Only show tooltips when the mouse intersects a chart element.
   */
  @Prop() tooltipIntersect: boolean = false;

  /**
   * Tooltip mode: `dataset`, `index`, `label`, `nearest`, `point`, `single`,
   * `x-axis`, `x`, or `y`. (See the
   * [interaction modes documentation](http://www.chartjs.org/docs/latest/general/interactions/modes.html#interaction-modes)
   * for details.)
   */
  @Prop() tooltipMode: InteractionMode;

  @State() datasets: any;

  protected queryChildDatasets() {
    return Array.from(this.el.querySelectorAll('rpc-dataset'));
  }

  protected async getData() {
    /* Query child dataset, and get data from dataSourceManager*/
    let childDatasets = this.queryChildDatasets();
    this.chartType.modifySourceData(this.parsedData, this.typeOptions);
    return this.combineData(childDatasets, this.parsedData);
  }

  createLabels(d) {
    return d.slice(1);
  }

  private createBackgroundColor(colors, index) {
    let backgroundColor;
    if (this.chartType.type === 'line' && this.fill != false) {
      backgroundColor = setOpacity(rpcColor(colors[index - 1]));
    } else if (this.chartType.type === 'pie' ||
        this.chartType.type === 'doughnut') {
      backgroundColor = rpcColor(colors);
    } else {
      backgroundColor = rpcColor(colors[index - 1])
    }
    return backgroundColor;
  }

  private injectChildDatasets(childDatasets, datasets) {
    for (let dataset of childDatasets) {
      datasets.splice(
        dataset.order,
        0,
        this.getDataset(dataset)
      )
    }
    return datasets;
  }

  protected combineData(childDatasets, data) {
    let dataObj = {};
    let datasets = [];
    let colors = toArray(this.colors);
    // transposed the data to make it easier to work with
    // https://stackoverflow.com/questions/17428587/transposing-a-2d-array-in-javascript
    let transposed = (data.length) ? 
      data[0].map((_col, i) => data.map(row => row[i])) : data;
    // Loop through data array to construct chartJS obj
    transposed.forEach((row: any[], i: number) =>
      i === 0 ?
      dataObj['labels'] = this.createLabels(row) :
      datasets.push(
        {
          'backgroundColor': this.createBackgroundColor(colors, i),
          'borderColor': rpcColor(colors[i - 1]),
          'borderWidth': (this.chartType.type === 'line') ? this.lineWidth : 0,
          'data': row.slice(1, row.length),
          'fill': this.fill,
          'label': row.slice(0, 1),
          'lineTension': this.lineTension
        }
      )
    );
    dataObj['datasets'] = this.injectChildDatasets(childDatasets, datasets);
    return dataObj;
  }

  protected getDataset(dataset: HTMLRpcDatasetElement) {
    return removeUndefined({
      backgroundColor: rpcColor(dataset.backgroundColor),
      borderColor: rpcColor(dataset.borderColor),
      borderWidth: dataset.borderWidth,
      data: toArray(dataset.data),
      fill: dataset.fill,
      label: dataset.label,
      lineTension: dataset.lineTension,
      pointRadius: dataset.pointRadius,
      type: dataset.type
    });
  }

  protected getXYScale(axis: 'x' | 'y') {
    // ? Not sure if this the correct way of fixing the concern of Matt for 
    // ? not creating a rpc-scale element. 
    let scale = {};
    scale['axis'] = axis;
    scale['gridLines'] = this.gridLines;
    scale['stacked'] = this.stacked;

    let label = this[axis + 'Label'];
    if (label) scale['scaleLabel'] = label;

    let xDefault : ScaleType =
      (this.chartType.type === 'horizontalBar') ? 'linear' : 'category';
    let yDefault : ScaleType =
      (this.chartType.type === 'horizontalBar') ? 'category' : 'linear';
    scale['type'] = (axis === 'x') ?
      this.xType || xDefault : this.yType || yDefault;

    scale['min'] = (axis === 'x') ? this.xMin : this.yMin;
    scale['max'] = (axis === 'x') ? this.xMax : this.yMax;

    return [scale];
  }

  protected getOptions() {
    // create default x, y scales
    let userScales = Array.from(this.el.querySelectorAll('rpc-scale'));
    let xScales: any[];
    let yScales: any[];
    xScales = [
      ...userScales.filter((s) => s.axis == 'x')
    ];
    yScales = [
      ...userScales.filter((s) => s.axis == 'y')
    ];

    // ? This part need review
    if (['line', 'bar', 'horizontalBar', 'bubble', 'scatter']
    .indexOf(this.chartType.type) > -1) {
      xScales = xScales.length === 0 ? this.getXYScale('x') : xScales;
      yScales = yScales.length === 0 ? this.getXYScale('y') : yScales;
    }

    let options : ChartOptions = {
      animation: {
        duration: this.animationDuration
      },
      aspectRatio: this.aspectRatio,
      legend: {
        display: this.legend,
        position: this.legendPosition
      },
      maintainAspectRatio: this.maintainAspectRatio,
      responsive: this.responsive,
      scales: {
        xAxes: this.getAxes(xScales),
        yAxes: this.getAxes(yScales)
      },
      title: {
        display: this.chartTitle != null,
        text: this.chartTitle,
        fontFamily:
          "'Montserrat', 'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
        fontSize: 18
      },
      tooltips: {
        enabled: this.tooltip,
        mode: this.tooltipMode ||
          (this.chartType.type === 'pie' ||
              this.chartType.type === 'doughnut') ?
            'nearest' : 'index',
        intersect: this.tooltipIntersect
      }
    };
    return removeUndefined(options);
  }

  protected getAxes(scales) {
    return scales.map((scale) => {
      return {
        display: scale.display,
        gridLines: {
          display: scale.gridLines
        },
        labels: toArray(scale.labels),
        ticks: {
          autoSkip: scale.autoSkip,
          max: scale.max,
          min: scale.min
        },
        offset: scale.offset,
        position: scale.position,
        scaleLabel: {
          display: scale.scaleLabel != null,
          labelString: scale.scaleLabel,
          fontStyle: 'bold'
        },
        stacked: scale.stacked,
        type: scale.type,
        time: {
          unit: scale.timeUnit
        },
        weight: scale.weight
      }
    });
  }

  protected async updateChart() {
    let config = {
      type: this.chartType.type,
      data: await this.getData(),
      options: this.getOptions()
    };
    this.chartType.modifyConfig(config, this.typeOptions);

    if (this.chart) this.chart.destroy();
    this.chart = new ChartJS(this.canvas.getContext('2d'), config);
  }

  @Watch('type')
  handleType() {
    this.chartType = chartTypeRegistry.get(this.type);
    this.chartType.modifyElement(this.el, this.typeOptions);
  }

  async componentDidRender() {
    await this.updateChart();
  }

  async componentWillLoad() {
    this.handleType();
    await this.parseData();
  }

  @Watch('data')
  @Watch('url')
  async parseData() {
    this.parsedData = await getDataForElement(this, true);
  }

  render() {
    let canvasAttrs = {};
    if (this.height) canvasAttrs['height'] = this.height;
    if (this.width) canvasAttrs['width'] = this.width;

    return ([
      <div class="chart-container"
          style={{position: 'relative', ...canvasAttrs}}>
        <canvas ref={(r) => this.canvas = r} style={canvasAttrs}>
          <rpc-table data={this.parsedData} tableTitle={this.chartTitle}>
          </rpc-table>
        </canvas>
      </div>,
      <rpc-metadata parent={this}></rpc-metadata>,
      <slot />
    ]);
  }
}
