import { ChartConfiguration, ChartType as ChartTypeString
  } from 'chart.js';


export interface ChartType {
    type: ChartTypeString;

    modifySourceData(data: any[][], _options?: any) : void;
    modifyElement(el: HTMLElement, _options?: any) : void;
    modifyConfig(config: ChartConfiguration, _options?: any) : void;
}
