import { Component, Prop } from '@stencil/core';
import { ChartType } from 'chart.js';


@Component({
  tag: 'rpc-dataset'
})
export class Dataset {
  /**
   * Fill color name or hexidecimal value.
   */
  @Prop() backgroundColor: string | string[];

  /**
   * Line color name or hexidecimal value.
   */
  @Prop() borderColor: string | string[];

  /**
   * Width of the line in pixels.
   */
  @Prop() borderWidth: number | number[];

  /**
   * Array or comma-separated string of dataset values.
   */
  @Prop() data: number[] | string;

  /**
   * Where to begin shading for an area chart created using the `line` chart
   * type: `start`, `end`, `origin`, or `false` (no shading).
   */
  @Prop() fill: boolean | number | string;

  /**
   * Dataset label used in the legend.
   */
  @Prop() label: string;

  /**
   * Bezier curve tension of the line. Use a line tension of `0` to draw
   * straight lines.
   */
  @Prop() lineTension: number;

  /**
   * Controls the order of this dataset compared to other datasets.
   */
  @Prop() order: number = 0;

  /**
   * Radius of the points in `line` or `scatter` chart types.
   */
  @Prop() pointRadius: number | number[];

  /**
   * Dataset type: `bar`, `bubble`, `doughnut`, `horizontalBar`,
   * `line`, `pie`, `polarArea`, `radar`, or `scatter`
   */
  @Prop() type: ChartType;
}
