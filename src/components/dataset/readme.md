# rpc-dataset

The `rpc-dataset` component can be used to create a custom dataset that is
added to the parent `rpc-chart`.

## Usage
In this example, a line dataset is overlaid on a bar chart:

```html
<rpc-chart url="data.csv"
  chart-title="Multiple Types Chart"
  x-label="Year"
  y-label="Score"
  type="bar">
  <rpc-dataset label="Baseline"
    type="line"
    data="4,4,4,4,4"
    fill="false"
    border-color="lime"
    point-radius="0"></rpc-dataset>
</rpc-chart>
```

<!-- Auto Generated Below -->


## Properties

| Property          | Attribute          | Description                                                                                                                      | Type                                                                                                           | Default     |
| ----------------- | ------------------ | -------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------- | ----------- |
| `backgroundColor` | `background-color` | Fill color name or hexidecimal value.                                                                                            | `string \| string[]`                                                                                           | `undefined` |
| `borderColor`     | `border-color`     | Line color name or hexidecimal value.                                                                                            | `string \| string[]`                                                                                           | `undefined` |
| `borderWidth`     | `border-width`     | Width of the line in pixels.                                                                                                     | `number \| number[]`                                                                                           | `undefined` |
| `data`            | `data`             | Array or comma-separated string of dataset values.                                                                               | `number[] \| string`                                                                                           | `undefined` |
| `fill`            | `fill`             | Where to begin shading for an area chart created using the `line` chart type: `start`, `end`, `origin`, or `false` (no shading). | `boolean \| number \| string`                                                                                  | `undefined` |
| `label`           | `label`            | Dataset label used in the legend.                                                                                                | `string`                                                                                                       | `undefined` |
| `lineTension`     | `line-tension`     | Bezier curve tension of the line. Use a line tension of `0` to draw straight lines.                                              | `number`                                                                                                       | `undefined` |
| `order`           | `order`            | Controls the order of this dataset compared to other datasets.                                                                   | `number`                                                                                                       | `0`         |
| `pointRadius`     | `point-radius`     | Radius of the points in `line` or `scatter` chart types.                                                                         | `number \| number[]`                                                                                           | `undefined` |
| `type`            | `type`             | Dataset type: `bar`, `bubble`, `doughnut`, `horizontalBar`, `line`, `pie`, `polarArea`, `radar`, or `scatter`                    | `"bar" \| "bubble" \| "doughnut" \| "horizontalBar" \| "line" \| "pie" \| "polarArea" \| "radar" \| "scatter"` | `undefined` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
