import { DataSource } from './datasource';
import * as d3Fetch from 'd3-fetch';


export class CSV extends DataSource {
  protected data: any[][];
  protected cache: any = {};
  private filterOption: string[] = 
    ['rows', 'columns', 'switch', 'cache'];

  public async getData(url: string, sanitize: boolean) {
    let parsed = this.parseQueryString(url);
    let baseUrl = parsed['baseUrl'];
    let cloneAndTransform = (data) => {
      data = [...data.map((row) => [...row])];
      return this.transformData(
        sanitize ? this.sanitizeData(data) : data, parsed
      );
    };
  
    if (parsed['cache'] === 'false')
      return cloneAndTransform(await this.fetchCSV(baseUrl));
  
    if (!this.cache[baseUrl]) this.cache[baseUrl] = this.fetchCSV(baseUrl);

    return cloneAndTransform(await this.cache[baseUrl]);
  }

  private parseQueryString(rawUrl: string) {
    let obj = {};
    let split = rawUrl.split('?');
    let queries;
    let baseUrl = split[0];
    if (split.length === 2) {
      queries = rawUrl.split('?')[1];

      for (let set of queries.split('&')) {
        let variable = set.split('=')[0];
  
        if (this.filterOption.indexOf(variable) === -1) {
          if (baseUrl.charAt(baseUrl.length - 1) != '?') {
            baseUrl += '?';
          }
          baseUrl += set;
        } else {
          obj[variable] = set.split('=')[1];
        }
      }
    }
    obj['baseUrl'] = baseUrl;
    return obj;
  }

  private async fetchCSV(resourceUrl: string) {
    let data: any[][];
    let res = await d3Fetch.csv(resourceUrl);
    data = res.map((row) => res.columns.map((col) => row[col]));
    data.unshift([...res.columns]);
    return data;
  }

  private toIntegers(text: string) {
    return text.split(',')
      .map((val) => parseInt(val))
      .filter((int) => !isNaN(int));
  }
  
  private transformData(data: any, paramObj: any) {
    if (paramObj['switch']) {
      data = data[0].map((_col, i) => data.map((row) => row[i]));
    }
    if (paramObj['rows']) {
      data = data.filter((_row, r) =>
        this.filterData(r, this.toIntegers(paramObj['rows']))
      );
    }
    if (paramObj['columns']) {
      data = data.map((row) =>
        row.filter((_col, c) =>
          this.filterData(c, this.toIntegers(paramObj['columns']))
        )
      );
    }
    return data;
  }
  
  protected filterData(i: number, values: number[]) {
    let pos = values[0] > 0;
    let sign = pos ? 1 : -1;
    let match = values.indexOf((i + 1) * sign) !== -1;
    return pos ? match : !match;
  }
  
  private sanitizeData(data) {
    return data.map((row: any[], i: number) =>
      i === 0 ? row :
        row.map((value: string, j: number) => 
          j === 0 ? value : parseFloat(value.replace(/[^0-9-\.]/g, '')))
    );
  }
}