export abstract class DataSource {
  abstract async getData(url: string, sanitize: boolean): Promise<any[][]>;
}
