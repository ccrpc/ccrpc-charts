import { CSV } from './csv';

interface dataSources {
  dataSource: any;
  regExp: RegExp;
}

export class DataSourceManager {
  protected dataSources: dataSources[] = [];
  public registerDataSource(dataSource: any, regExp: RegExp) {
    this.dataSources.push({ dataSource: dataSource, regExp: regExp });
  }
  public async getData(url: string, sanitize: boolean) {
    for (let set of this.dataSources) {
      if (url.match(set.regExp) !== null) {
        return await set.dataSource.getData(url, sanitize);
      }
    }
  }
}

export let dataSourceManager = new DataSourceManager();
dataSourceManager.registerDataSource(
  new CSV(),
  /\.csv($|\?)/
);
