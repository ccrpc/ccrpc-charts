# Data Sources
CCRPC Charts provides a pluggable data source API for retrieving data from
a URL. A CSV data source is included with the library, and developers
can add their own custom data sources.

## Built-in Data Sources

### CSV Data Source
The CSV data source matches any URL that ends with `.csv` or contains `.csv?`.
It accepts three optional configuration parameters, which can be added to
the query string of the CSV URL:

* `switch`: switch rows and columns: `false` (default) or `true`
* `rows`: comma-separated list of row numbers to include. Negative numbers
  can be used to remove the corresponding rows.
  Row selection occurs after row/column switching, if enabled.
* `columns`: comma-separated list of column numbers to include.
  Negative numbers can be used to remove the corresponding columns.
  Column selection occurs after row/column switching, if enabled.

By default, CSV results are cached and reused if they appear multiple times
on the same page. To disable this behavior, add `cache=false` to the query
string for the CSV.

#### Examples
Include only columns 2 and 3:
```
example.csv?columns=2,3
```

Exclude row 4:
```
example.csv?rows=-4
```

Transpose rows and columns, then include only columns 1 and 2:
```
example.csv?switch=true&columns=1,2
```

Disable caching:
```
example.csv?cache=false
```

## Creating Data Sources
Custom data sources can be implemented by extending the `DataSource` class
and overriding the `getData` method:

```ts
import { DataSource } from '@ccrpc/charts';

class MyDataSource extends DataSource {

  /**
   * Retrieve the data for the given URL. Caching should be implemented
   * by the data source, if desired. The method should return a promise
   * that resolves to an array of arrays, where the first array contains
   * column names, and subsequent arrays contain column values. If
   * sanitize is true, the returned values should be be cast to the appropriate
   * types for display in a chart (e.g., numbers, dates, etc.).
   */
  async getData(url: string, sanitize: boolean): Promise<any[][]> {}
}
```

New custom data sources have to be registered with CCRPC Charts before
they can be used in `rpc-chart` or `rpc-table` components. The registration
associates an instance of the data source class with a regular expression
used to match URLs for the data source.

```ts
import { dataSourceManager } from '@ccrpc/charts';

dataSourceManager.registerDataSource(new MyDataSource, /\.mds($|\?)/);
```
