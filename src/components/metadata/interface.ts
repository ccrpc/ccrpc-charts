import { DataElement } from '../../utils';


export interface MetaElement extends DataElement {
  description?: string;
  download?: boolean | string;
  source?: string;
  sourceUrl?: string;
}
