import { Component, Prop, h } from '@stencil/core';
import { downloadDataForElement } from '../../utils';
import { MetaElement } from './interface';


@Component({
  styleUrl: 'metadata.css',
  tag: 'rpc-metadata'
})
export class Metadata {

  /**
   * The `rpc-chart` or `rpc-table` element that provides the data.
   */
  @Prop() parent: MetaElement;

  getDescription() {
    if (!this.parent.description) return;
    return (<p class="description">{this.parent.description}</p>);
  }

  getSource() {
    if (!this.parent.source) return;
    let source = (this.parent.sourceUrl) ?
      <a href={this.parent.sourceUrl}>{this.parent.source}</a> :
      this.parent.source;
    return (<p class="source"><strong>Source:</strong> {source}</p>);
  }

  getDownload() {
    if (!this.parent.download || this.parent.download === 'false') return;

    let url = this.parent.download;
    let clickHandler = (_e) => {};
    if (typeof this.parent.download === 'boolean') {
      url = '#';
      clickHandler = (e) => {
        e.preventDefault();
        downloadDataForElement(this.parent);
      };
    }

    return (
      <p>
        <strong>Download: </strong>
        <a href={url.toString()} onClick={clickHandler}>
          CSV
          <span class="sr-only">
            for {this.parent.tableTitle || this.parent.chartTitle}
          </span>
        </a>
      </p>
    );
  }

  render() {
    return [
      this.getDescription(),
      this.getSource(),
      this.getDownload()
    ];
  }
}
