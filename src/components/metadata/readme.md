# rpc-metadata



<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description                                                    | Type          | Default     |
| -------- | --------- | -------------------------------------------------------------- | ------------- | ----------- |
| `parent` | --        | The `rpc-chart` or `rpc-table` element that provides the data. | `MetaElement` | `undefined` |


## Dependencies

### Used by

 - [rpc-chart](../chart)
 - [rpc-table](../table)

### Graph
```mermaid
graph TD;
  rpc-chart --> rpc-metadata
  rpc-table --> rpc-metadata
  style rpc-metadata fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
