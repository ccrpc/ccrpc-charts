# rpc-scale



<!-- Auto Generated Below -->


## Properties

| Property     | Attribute     | Description                                                                                                                   | Type                                                                                                   | Default     |
| ------------ | ------------- | ----------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------ | ----------- |
| `autoSkip`   | `auto-skip`   | Automatically drop some labels when space is limited.                                                                         | `boolean`                                                                                              | `true`      |
| `axis`       | `axis`        | Axis for which the scale should be used.                                                                                      | `"x" \| "y"`                                                                                           | `undefined` |
| `display`    | `display`     | Show the scale.                                                                                                               | `boolean`                                                                                              | `true`      |
| `gridLines`  | `grid-lines`  | Show grid lines on charts with an x-axis and y-axis.                                                                          | `boolean`                                                                                              | `true`      |
| `labels`     | `labels`      | An array or comma-separated string of tick labels.                                                                            | `string \| string[]`                                                                                   | `undefined` |
| `max`        | `max`         | Maximum tick value.                                                                                                           | `number`                                                                                               | `undefined` |
| `min`        | `min`         | Minimum tick value.                                                                                                           | `number`                                                                                               | `undefined` |
| `offset`     | `offset`      | Add extra space between the ends of the scale and the chart edge.                                                             | `boolean`                                                                                              | `undefined` |
| `position`   | `position`    | Side of the chart where the scale will be displayed: `left`, `right`, `bottom`, or `top`                                      | `"bottom" \| "chartArea" \| "left" \| "right" \| "top"`                                                | `undefined` |
| `scaleLabel` | `scale-label` | Label for the entire scale.                                                                                                   | `string`                                                                                               | `undefined` |
| `stacked`    | `stacked`     | Stack bars in `bar` or `horizontalBar` chart types.                                                                           | `boolean`                                                                                              | `undefined` |
| `timeUnit`   | `time-unit`   | Time unit for scales of type `time`: `millisecond`, `second`, `minute`, `hour`, `day`, `week`, `month`, `quarter`, or `year`. | `"day" \| "hour" \| "millisecond" \| "minute" \| "month" \| "quarter" \| "second" \| "week" \| "year"` | `undefined` |
| `type`       | `type`        | Scale type: `category`, `linear`, `logarithmic`, `radialLinear`, or `time`.                                                   | `"category" \| "linear" \| "logarithmic" \| "radialLinear" \| "time"`                                  | `undefined` |
| `weight`     | `weight`      | Used to sort scales. Scales with height numbers are placed farther from the chart.                                            | `number`                                                                                               | `0`         |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
