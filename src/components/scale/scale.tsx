import { Component, Prop } from '@stencil/core';
import { PositionType, ScaleType, TimeUnit } from 'chart.js';


@Component({
  tag: 'rpc-scale'
})
export class Scale {
  /**
   * Automatically drop some labels when space is limited.
   */
  @Prop() autoSkip: boolean = true;

  /**
   * Axis for which the scale should be used.
   */
  @Prop() axis: 'x' | 'y';

  /**
   * Show the scale.
   */
  @Prop() display: boolean = true;

  /**
   * Show grid lines on charts with an x-axis and y-axis.
   */
  @Prop() gridLines: boolean = true;

  /**
   * An array or comma-separated string of tick labels.
   */
  @Prop() labels: string[] | string;

  /**
   * Maximum tick value.
   */
  @Prop() max: number;

  /**
   * Minimum tick value.
   */
  @Prop() min: number;

  /**
   * Add extra space between the ends of the scale and the chart edge.
   */
  @Prop() offset: boolean;

  /**
   * Side of the chart where the scale will be displayed: `left`,
   * `right`, `bottom`, or `top`
   */
  @Prop() position: PositionType;

  /**
   * Label for the entire scale.
   */
  @Prop() scaleLabel: string;

  /**
   * Stack bars in `bar` or `horizontalBar` chart types.
   */
  @Prop() stacked: boolean;

  /**
   * Scale type: `category`, `linear`, `logarithmic`, `radialLinear`, or `time`.
   */
  @Prop() type: ScaleType;

  /**
   * Time unit for scales of type `time`: `millisecond`, `second`, `minute`,
   * `hour`, `day`, `week`, `month`, `quarter`, or `year`.
   */
  @Prop() timeUnit: TimeUnit;

  /**
   * Used to sort scales. Scales with height numbers are placed farther from
   * the chart.
   */
  @Prop() weight: number = 0;
}
