# rpc-table

The `rpc-table` component creates a table using data from a CSV file.

## Usage
```html
<rpc-table url="data.csv"
  table-title="Table Example"
  table-subtitle="Team Results, 2001 - 2005"
  source="CCRPC"
  source-url="https://ccrpc.org/"
  text-alignment="l,r"></rpc-table>
```

<!-- Auto Generated Below -->


## Properties

| Property        | Attribute        | Description                                                                                                                                                                                                    | Type                        | Default     |
| --------------- | ---------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------------- | ----------- |
| `columns`       | `columns`        | <span style="color:red">**[DEPRECATED]**</span> See [Data Sources](../datasource) for URL-based filtering options.<br/><br/>                                                                                   | `number[] \| string`        | `undefined` |
| `data`          | `data`           | An array of row arrays, or a JSON-formatted string containing an array of row arrays. If the `url` property is set, `data` is ignored.                                                                         | `any[][] \| string`         | `[]`        |
| `description`   | `description`    | Summary text that appears below the table.                                                                                                                                                                     | `string`                    | `undefined` |
| `download`      | `download`       | Show the data download link. If a URL is provided, it is used as the URL for the download. Otherwise, if the value of download is `true`, the data returned by the data source are converted to a CSV.         | `boolean \| string`         | `true`      |
| `footer`        | `footer`         | Number of rows that should be formatted as footers.                                                                                                                                                            | `number`                    | `0`         |
| `header`        | `header`         | Number of rows that should be formatted as headers.                                                                                                                                                            | `number`                    | `1`         |
| `rows`          | `rows`           | <span style="color:red">**[DEPRECATED]**</span> See [Data Sources](../datasource) for URL-based filtering options.<br/><br/>                                                                                   | `number[] \| string`        | `undefined` |
| `scrollY`       | `scroll-y`       | Show a horizontal scroll bar if the table is too wide for the content area.                                                                                                                                    | `boolean`                   | `true`      |
| `source`        | `source`         | Name of the data source displayed below the table.                                                                                                                                                             | `string`                    | `undefined` |
| `sourceUrl`     | `source-url`     | URL for the data source link.                                                                                                                                                                                  | `string`                    | `undefined` |
| `switch`        | `switch`         | <span style="color:red">**[DEPRECATED]**</span> See [Data Sources](../datasource) for URL-based filtering options.<br/><br/>                                                                                   | `boolean`                   | `undefined` |
| `tableSubtitle` | `table-subtitle` | Subtitle displayed at the top of the table.                                                                                                                                                                    | `string`                    | `undefined` |
| `tableTitle`    | `table-title`    | Title displayed at the top of the table.                                                                                                                                                                       | `string`                    | `undefined` |
| `textAlignment` | `text-alignment` | Comma-separated list of alignment values: `left` or `l`, `center` or `c`, and `right` or `r`. If the list is shorter than the number of columns, the final alignment value is used for all subsequent columns. | `TextAlignemnt[] \| string` | `'l'`       |
| `url`           | `url`            | Data source URL. For more information, see [Data Sources](../datasource).                                                                                                                                      | `string`                    | `undefined` |


## Dependencies

### Used by

 - [rpc-chart](../chart)

### Depends on

- [rpc-metadata](../metadata)

### Graph
```mermaid
graph TD;
  rpc-table --> rpc-metadata
  rpc-chart --> rpc-table
  style rpc-table fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
