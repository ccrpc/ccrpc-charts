import { h, Component, Prop, State, Watch } from '@stencil/core';
import { TextAlignemnt } from './interface';
import { toArray, getDataForElement } from '../../utils';


@Component({
  tag: 'rpc-table',
  styleUrl: 'table.css',
  shadow: true
})
export class Table {
  @State() parsedData?: string[][] = [];

  /**
   * Data source URL. For more information, see [Data Sources](../datasource).
   */
  @Prop() url: string;

  /**
   * An array of row arrays, or a JSON-formatted string containing an array
   * of row arrays. If the `url` property is set, `data` is ignored.
   */
  @Prop() data: string | any[][] = [];

  /**
   * @deprecated See [Data Sources](../datasource) for URL-based filtering
   * options.
   */
  @Prop() rows: number[] | string;

  /**
   * @deprecated See [Data Sources](../datasource) for URL-based filtering
   * options.
   */
  @Prop() columns: number[] | string;

  /**
   * @deprecated See [Data Sources](../datasource) for URL-based filtering
   * options.
   */
  @Prop() switch: boolean;

  /** 
   * Show the data download link. If a URL is provided, it is used as the
   * URL for the download. Otherwise, if the value of download is `true`,
   * the data returned by the data source are converted to a CSV.
   */
  @Prop() download: boolean | string = true;

  /**
   * Summary text that appears below the table.
   */
  @Prop() description: string;

  /**
   * Number of rows that should be formatted as headers.
   */
  @Prop() header: number = 1;

  /**
   * Number of rows that should be formatted as footers.
   */
  @Prop() footer: number = 0;

  /**
   * Show a horizontal scroll bar if the table is too wide for the
   * content area.
   */
  @Prop() scrollY: boolean = true;

  /**
   * Name of the data source displayed below the table.
   */
  @Prop() source: string;

  /**
   * URL for the data source link.
   */
  @Prop() sourceUrl: string;

  /**
   * Title displayed at the top of the table.
   */
  @Prop() tableTitle: string;

  /**
   * Subtitle displayed at the top of the table.
   */
  @Prop() tableSubtitle: string;

  /**
   * Comma-separated list of alignment values: `left` or `l`,
   * `center` or `c`, and `right` or `r`. If the list is shorter than the
   * number of columns, the final alignment value is used for all
   * subsequent columns.
   */
  @Prop() textAlignment: TextAlignemnt[] | string = 'l';

  async componentWillLoad() {
    await this.parseData();
  }

  @Watch('data')
  @Watch('url')
  async parseData() {
    this.parsedData = await getDataForElement(this, false);
  }

  getCaption() {
    let items = [];
    if (this.tableTitle)
      items.push(<span class="title">{this.tableTitle}</span>);
    if (this.tableSubtitle)
      items.push(<span class="subtitle">{this.tableSubtitle}</span>);
    if (items.length) return (<caption>{items}</caption>);
  }

  getRow(data: string[], Cell: 'th' | 'td') {
    let align = toArray(this.textAlignment).map((a) => {
      return {
        'c': 'center',
        'l': 'left',
        'r': 'right'
      }[a] || a;
    });
    let defaultAlign = align[align.length - 1] || 'left';

    let cells = data.map((value, i) =>
      <Cell class={'align-' + (align[i] || defaultAlign)}>{value}</Cell>);
    return (<tr>{cells}</tr>);
  }

  getSection(data: string[][], Parent: string, Cell: 'th' | 'td') {
    if (!data.length) return;
    let rows = data.map((row) => this.getRow(row, Cell));
    return (<Parent>{rows}</Parent>);
  }

  render() {
    let body = [...this.parsedData];
    let head = body.splice(0, this.header);
    let foot = body.splice(body.length - 1 - this.footer, this.footer);

    return ([
      <div class={`wrapper ${(this.scrollY) ? 'scroll-y' : 'no-scroll-y'}`}>
        <table>{[
          this.getCaption(),
          this.getSection(head, 'thead', 'th'),
          this.getSection(body, 'tbody', 'td'),
          this.getSection(foot, 'tfoot', 'td')]}
        </table>
      </div>,
      <rpc-metadata parent={this}></rpc-metadata>
    ]);
  }
}
