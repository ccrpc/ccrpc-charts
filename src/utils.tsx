import Color from 'chartjs-color';
import { dataSourceManager } from './components/datasource/datasourcemanager';


export interface DataElement {
  data?: string | any[][];
  url?: string;
  switch?: boolean;
  rows?: string | number[];
  columns?: string | number[];
  chartTitle?: string;
  tableTitle?: string;
}

export async function getDataForElement(el: DataElement, sanitize: boolean) {
  let data: any[][];

  if (el.url) {
    let url = el.url;

    // Handle deprecated properties.
    let qs = ['switch', 'rows', 'columns']
      .filter((prop) => el[prop])
      .map((prop) => `${prop}=${el[prop].toString()}`)
      .join('&');

    if (qs) {
      url += ((url.indexOf('?') === -1) ? '?' : '&') + qs;
      console.warn('Properties switch, rows, and columns are deprecated. ' +
        `Use url="${url}" instead.`);
    }

    data = await dataSourceManager.getData(url, sanitize);
  } else if (typeof el.data === 'string') {
    data = JSON.parse(el.data);
  } else {
    data = el.data;
  }

  if (!Array.isArray(data) || !data.every(Array.isArray)) {
    console.warn('Data should be an array of arrays (e.g., [[1, 2], [3, 4]]).');
    return [];
  }  
  return data;
}

function dataToCsvString(data: any[][]) {
  return data.map((row) => {
    return row.map((value) => {
      let text = (value === null || value === undefined) ?
        '' : value.toString();
      return (text.indexOf(',') === -1) ? text :
        `"${text.replace('"', '\"')}"`;
    }).join(',')
  }).join('\r\n');
}

function makeFilename(text, ext) {
  return `${text.toLowerCase().replace(/[^A-Za-z0-9]+/g, '-')}.${ext}`;
}

export async function downloadDataForElement(el: DataElement) {
  const data = await getDataForElement(el, false);
  const blob = new Blob([dataToCsvString(data)], {type : 'text/csv'});
  const filename =
    makeFilename(el.chartTitle || el.tableTitle || 'data', 'csv');

  if (window.navigator.msSaveBlob !== undefined) {
    // IE hack
    window.navigator.msSaveBlob(blob, filename);
  } else {
    const link = document.createElement('a');
    link.href = window.URL.createObjectURL(blob);
    link.download = filename;
    link.click();
  }
}

export function toArray(values: string | any[], trim=true) {
  values = (typeof values === 'string') ? values.split(',') : values;
  if (Array.isArray(values) && trim) values = values.map(
    (value) => (typeof value === 'string') ? value.trim() : value);
  return values;
}

export function removeUndefined(obj: any) {
  for (let key in obj) {
    if (obj[key] && typeof obj[key] === 'object') removeUndefined(obj[key]);
    else if (obj[key] === undefined) delete obj[key];
  }
  return obj;
}

export function setOpacity(color : string, opacity : number = 0.5) {
  return Color(color).alpha(1 - opacity).rgbString();
}

export function rpcColor(color: string | string[]) {
  let colors = {
    red: '#be1e2d',
    blue: '#24abe2',
    lime: '#8dc53f',
    orange: '#f6921e',
    indigo: '#2e3191',
    green: '#009345',
    violet: '#9122bf',
    yellow: '#edd95f',
    gray: '#848484',
    brown: '#754d3f'
  };
  if (color === undefined) return color;
  return (typeof color === 'string') ?
    colors[color] || color : color.map((c) => colors[c] || c);
}

export function wrap(text: string | string[], chars: number) {
  if (!chars || typeof text !== 'string') return text;
  if (text.length <= chars) return text;

  let lines = [];
  let line = '';
  for (let word of text.split(' ')) {
    if (line.length && line.length + word.length >= chars) {
      lines.push(line);
      line = word;
    } else {
      line += ' ' + word;
    }
  }
  if (line.length) lines.push(line);
  return lines;
}
