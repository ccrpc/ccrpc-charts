import { Config } from '@stencil/core';

export const config: Config = {
  namespace: 'ccrpc-charts',
  outputTargets:[
    {
      type: 'dist',
      esmLoaderPath: '../loader'
    },
    {
      type: 'www',
      baseUrl: '/ccrpc-charts',
      serviceWorker: null
    },
    {
      type: 'docs-readme'
    }
  ]
};
